import { maximumProfit } from "./profit"
import theoretically from 'jest-theories';

describe('maximumProfitTests', () => {
    const theories = [
        {input: [8, 5, 12, 9, 19, 1], expected: 14},
        {input: [2, 4, 9, 3, 8], expected: 7},
        {input: [21, 12, 11, 9, 6, 3], expected: 0},
    ]

    theoretically('the biggest diff in [{input}] is {expected}', theories, theory => {
        const output = maximumProfit(theory.input);
        expect(output).toBe(theory.expected);
    })
});