export function maximumProfit(stocks) {
    let greatestDifference = 0
    let index = 1

    for (const stock of stocks) {

        for (let i = index; i < stocks.length; i++) {
            if (stocks[i] - stock > greatestDifference)
                greatestDifference = stocks[i] - stock
        }
        index++
    }
    return greatestDifference
}