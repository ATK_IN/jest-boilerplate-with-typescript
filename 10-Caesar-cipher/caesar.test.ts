import { caesarCipher } from "./caesar"
import theoretically from 'jest-theories';

describe('cipherTests', () => {
	const theories = [
		{ inputString: "Always-Look-on-the-Bright-Side-of-Life", inputNumber: 5, expected: "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj" },
		{ inputString: "A friend in need is a friend indeed", inputNumber: 20, expected: "U zlcyhx ch hyyx cm u zlcyhx chxyyx" },
		{ inputString: "A friend in need is a friend indeed", inputNumber: 0, expected: "A friend in need is a friend indeed" },
	]

	theoretically('{inputString} rotated with {inputNumber} should be encrypted to {expected}',
		theories, theory => {
			const output = caesarCipher(theory.inputString, theory.inputNumber);
			expect(output).toStrictEqual(theory.expected);
		})
})