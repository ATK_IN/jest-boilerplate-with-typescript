export function caesarCipher(s: string, k: number) {
	let returnString = ""
	let upper = false
	for (let letter of s) {
		if (letter.match(/[A-Z]/)) {
			upper = true
			letter = letter.toLowerCase()
		}

		let index = alphabet.indexOf(letter)
		if (index === -1) {
			returnString += letter
			continue
		}

		index += k
		if (index > alphabet.length) {
			index -= alphabet.length
		}
		if (upper)
			returnString += alphabet[index].toUpperCase()
		else
			returnString += alphabet[index]

		upper = false
	}
	return returnString
}

const alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]