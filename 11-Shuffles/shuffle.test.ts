import { shuffleCount } from "./shuffle"
import theoretically from 'jest-theories';

describe('shuffleTests', () => {
	const theories = [
		{ input: 8,expected: 3 },
		{ input: 14,expected: 12 },
		{ input: 52,expected: 8 },
		{ input: 13,expected: 1 },
	]

	theoretically('Deck with {input} cards should take {expected} shuffles to return',
		theories, theory => {
			const output = shuffleCount(theory.input);
			expect(output).toStrictEqual(theory.expected);
		})
})