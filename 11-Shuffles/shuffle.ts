export function shuffleCount(size: number) {
	if (size % 2 != 0)
		return 1

	let deck = []
	for (let card = 1; card <= size; card++) 
		deck.push(card)
	
	let count = 0
	let shuffledDeck = [...deck]
	do {
		shuffledDeck = shuffle(shuffledDeck)
		count++
	} while (JSON.stringify(deck) != JSON.stringify(shuffledDeck));

	return count
}

function shuffle(deck: number[]) {
	let returnArray = []
	let index = 0

	for (let card = 0; card < (deck.length / 2); card++) {
		returnArray[index] = deck[card]
		index += 2
	}

	index = 1
	for (let card = (deck.length / 2); card < deck.length; card++) {
		returnArray[index] = deck[card]
		index += 2
	}

	return returnArray
}