let turn = 0
let board = [
	null, null, null,
	null, null, null,
	null, null, null
]
let won = false
let currentPlayerElement = document.getElementById("currentPlayer")

function handleClick(id) {
	let buttonImageElement = document.getElementById(`img${id}`)
	let buttonElement = document.getElementById(id)
	buttonImageElement.src = `images/${turn}.png`
	buttonElement.disabled = true
	board[id] = turn
	hasWon()

	if (turn == 1){
		turn = 0
		currentPlayerElement.innerText = "O"
	}
	else{
		turn++
		currentPlayerElement.innerText = "X"
	}

}

function hasWon() {
	if (board[0] === board[1] && board[1] === board[2] && board[2] !== null)
		won = true
	if (board[3] === board[4] && board[4] === board[5] && board[5] !== null)
		won = true
	if (board[6] === board[7] && board[7] === board[8] && board[8] !== null)
		won = true
	if (board[0] === board[4] && board[4] === board[8] && board[8] !== null)
		won = true
	if (board[2] === board[4] && board[4] === board[6] && board[6] !== null)
		won = true
	if (board[0] === board[3] && board[3] === board[6] && board[6] !== null)
		won = true
	if (board[1] === board[4] && board[4] === board[7] && board[7] !== null)
		won = true
	if (board[2] === board[5] && board[5] === board[8] && board[8] !== null)
		won = true

	if (won) {
		alert(`Player ${turn + 1} has won!`)
		for (let index = 0; index < board.length; index++) {
			if (board[index] === null) {
				buttonElement = document.getElementById(index)
				buttonElement.disabled = true
			}
		}
	}
}