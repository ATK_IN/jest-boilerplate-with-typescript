import { splitOnDoubleLetter } from "./splitter"
import theoretically from 'jest-theories';

describe('splitterTests', () => {
    const theories = [
        { input: "Letter", expected: ["let", "ter"] },
        { input: "Really", expected: ["real", "ly"] },
        { input: "Happy", expected: ["hap", "py"] },
        { input: "Shall", expected: ["shal", "l"] },
        { input: "Tool", expected: ["to", "ol"] },
        { input: "Mississippi", expected: ["mis", "sis", "sip", "pi"] },
        { input: "AAAA", expected: ["a","a","a","a"] },
        { input: "Easy", expected: [] },
    ]

    theoretically('the word [{input}] should split to {expected}', theories, theory => {
        const output = splitOnDoubleLetter(theory.input);
        expect(output).toStrictEqual(theory.expected);
    })
});