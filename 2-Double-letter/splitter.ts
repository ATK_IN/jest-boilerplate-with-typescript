export function splitOnDoubleLetter(word) {
    word = word.toLowerCase()
    let returnArray = []

    let previousLetters = ''
    for (const letter of word) {
        if (previousLetters.endsWith(letter)) {
            returnArray.push(previousLetters)
            previousLetters = letter
            continue
        }
        previousLetters += letter
    }

    if (word == previousLetters)
        return []

    returnArray.push(previousLetters)

    return returnArray
}