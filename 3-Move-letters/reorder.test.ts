import { reorder } from "./reorder"
import theoretically from 'jest-theories';

describe('reorderTests', () => {
    const theories = [
        { input: "hA2p4Py", expected: "APhpy24" },
        { input: "m11oveMENT", expected: "MENTmove11" },
        { input: "s9hOrt4CAKE", expected: "OCAKEshrt94" },
    ]

    theoretically("'{input}' should be reordered to '{expected}'",
        theories, theory => {
            const output = reorder(theory.input);
            expect(output).toStrictEqual(theory.expected);
        })
});