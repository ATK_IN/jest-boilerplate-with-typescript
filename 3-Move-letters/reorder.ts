export function reorder(word:string) {
    let upperCase = ""
    let lowerCase = ""
    let numbers = ""
    
    for (const letter of word) {
        if (letter.match(/[A-Z]/))
            upperCase += letter
        if (letter.match(/[a-z]/))
            lowerCase += letter
        if (letter.match(/[0-9]/))
            numbers += letter
    }

    return upperCase + lowerCase + numbers
}