import { elasticize } from "./elastic"
import theoretically from 'jest-theories';

describe('elasticTests', () => {
    const theories = [
        { input: "ANNA", expected: "ANNNNA" },
        { input: "KAYAK", expected: "KAAYYYAAK" },
        { input: "X", expected: "X" },
    ]

    theoretically("'{input}' should be stretched to '{expected}'",
        theories, theory => {
            const output = elasticize(theory.input);
            expect(output).toStrictEqual(theory.expected);
        })
});