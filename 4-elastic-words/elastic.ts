export function elasticize(word: string) {
    if (word.length < 3)
        return word

    let reverseIndex = word.length / 2
    let count = 0
    let stretchedWord = ""

    for (let index = 0; index < word.length; index++) {
        if (index < reverseIndex)
            count++
        if (index > reverseIndex)
            count--

        for (let i = 0; i < count; i++) {
            stretchedWord += word.charAt(index)
        }
    }

    return stretchedWord
}
