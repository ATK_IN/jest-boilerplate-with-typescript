import { rearrange } from "./rearrange"
import theoretically from 'jest-theories';

describe('rearrangeTests', () => {
    const theories = [
        { input: "is2 Thi1s T4est 3a", expected: "This is a Test" },
        { input: "4of Fo1r pe6ople g3ood th5e the2", expected: "For the good of the people" },
        { input: "5weird i2s JavaScri1pt dam4n so3", expected: "JavaScript is so damn weird" },
        { input: "", expected: "" },
        { input: " ", expected: "" },
    ]

    theoretically('"{input}" should be rearanged to "{expected}"',
        theories, theory => {
            const output = rearrange(theory.input);
            expect(output).toStrictEqual(theory.expected);
        })
});