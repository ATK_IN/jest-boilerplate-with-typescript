export function rearrange(sentence: string) {
    if (sentence.trim().length == 0)
        return ""

    let rearrangedSentence = []

    for (let word of sentence.split(" ")) {
        let index = word.match(/[1-9]/)[0]

        rearrangedSentence[parseInt(index) - 1] = word.replace(index, "")
    }

    return rearrangedSentence.join(" ")
}
