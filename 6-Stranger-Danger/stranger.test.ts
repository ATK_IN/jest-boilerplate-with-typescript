import { noStrangers } from "./stranger"
import theoretically from 'jest-theories';

describe('strangerTests', () => {
    const theories = [
        {
            input: "See Spot run. See Spot jump. Spot likes jumping. See Spot fly.",
            expected: [["spot", "see"], []]
        },
        {
            input: "See Spot run. See Spot jump. Spot likes jumping. See Spot fly. See Spot run, run Spot, jump Spot. See Spot jump.",
            expected: [["run", "jump"], ["spot", "see"]]
        },
        {
            input: "Let's go eat. Let's go. Let's eat",
            expected: [["let's"], []]
        }
    ]

    theoretically('"{input}" should give "[{expected}]"',
        theories, theory => {
            const output = noStrangers(theory.input);
            expect(output).toStrictEqual(theory.expected);
        })
});