export function noStrangers(sentence: string) {
    const words = {}
    const acquaintances = []
    const friends = []

    for (const word of sentence.replace(/[^\w\s']/g, "").toLowerCase().split(" ")) {
        words[word] = words[word] + 1 || 1

        if (words[word] == 3) 
            acquaintances.push(word)
        
        if (words[word] == 5) {
            friends.push(word)
            acquaintances.splice(acquaintances.indexOf(word), 1)
        }
    }

    return [acquaintances, friends]
}
