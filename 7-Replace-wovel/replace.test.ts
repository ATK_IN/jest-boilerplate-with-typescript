import { replaceWithFor, replaceWithMap } from "./replace"
import theoretically from 'jest-theories';

describe('replaceWithForTests', () => {
    const theories = [
        { input: "karAchi", expected: "k1r1ch3" },
        { input: "chEmBur", expected: "ch2mb5r" },
        { input: "khandbari", expected: "kh1ndb1r3" },
        { input: "LexiCAl", expected: "l2x3c1l" },
        { input: "fuNctionS", expected: "f5nct34ns" },
        { input: "EASY", expected: "21sy" }
    ]

    theoretically('ReplaceWithFor({input}) should return "{expected}"',
        theories, theory => {
            const output = replaceWithFor(theory.input);
            expect(output).toStrictEqual(theory.expected);
        })
});
describe('replaceWithMapTests', () => {
    const theories = [
        { input: "karAchi", expected: "k1r1ch3" },
        { input: "chEmBur", expected: "ch2mb5r" },
        { input: "khandbari", expected: "kh1ndb1r3" },
        { input: "LexiCAl", expected: "l2x3c1l" },
        { input: "fuNctionS", expected: "f5nct34ns" },
        { input: "EASY", expected: "21sy" }
    ]

    theoretically('ReplaceWithMap({input}) should return "{expected}"',
        theories, theory => {
            const output = replaceWithMap(theory.input);
            expect(output).toStrictEqual(theory.expected);
        })
});