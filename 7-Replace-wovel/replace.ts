export function replaceWithFor(word: string) {
    let letterList = []
    word = word.toLowerCase()

    for (let index = 0; index < word.length; index++)
        letterList.push(replacements[word[index]] || word[index])

    return letterList.join("")
}

export function replaceWithMap(word: string) {
    return word
        .toLowerCase()
        .split("")
        .map((letter) => replacements[letter] || letter)
        .join("")
}

const replacements = {
    a: 1,
    e: 2,
    i: 3,
    o: 4,
    u: 5
}