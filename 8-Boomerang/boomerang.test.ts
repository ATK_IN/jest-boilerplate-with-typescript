import { countBoomerangs } from "./boomerang"
import theoretically from 'jest-theories';

describe('boomerangTests', () => {
	const theories = [
		{ input: [9, 5, 9, 5, 1, 1, 1], expected: 2 },
		{ input: [5, 6, 6, 7, 6, 3, 9], expected: 1 },
		{ input: [4, 4, 4, 9, 9, 9, 9], expected: 0 },
		{ input: [1, 7, 1, 7, 1, 7, 1], expected: 5 },
		{ input: [1, 7], expected: 0 }
	]

	theoretically('[{input}] should have {expected} boomerangs',
		theories, theory => {
			const output = countBoomerangs(theory.input);
			expect(output).toStrictEqual(theory.expected);
		})
});