export function countBoomerangs(numbers: number[]) {
	if (numbers.length < 3)
		return 0

	let boomerangCount = 0
	for (let index = 0; index < numbers.length - 1; index++) {
		if (numbers[index] !== numbers[index + 1] &&
			numbers[index] === numbers[index + 2])
			boomerangCount++
	}

	return boomerangCount
}
