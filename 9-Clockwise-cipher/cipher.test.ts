import { clockwiseCipher } from "./cipher"
import theoretically from 'jest-theories';

describe('cipherTests', () => {
	const theories = [
		{ input: "It's time to go on an adventure",
		 expected: "I e otanvu t    e ot  rgmdenans oti'" },
		{ input: "Edabit is amazing", expected: "Eisadng  tm    i   zbia a" },
		{ input: "Mubashir Hassan", expected: "Ms ussahr nHaaib" },
		{ input: "Matt MacPherson", expected: "M ParsoMc nhteat" },
		{ input: "Grophlyn", expected: "Ghrn lpyo" },
		{ input: "bob", expected: "bo b" },
		{ input: "b", expected: "b" }
	]

	theoretically('{input} should be encrypted to {expected}',
		theories, theory => {
			const output = clockwiseCipher(theory.input);
			expect(output).toStrictEqual(theory.expected);
		})
})