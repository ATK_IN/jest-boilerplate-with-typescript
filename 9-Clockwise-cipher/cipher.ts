export function clockwiseCipher(word: string) {
	if (word.length === 1)
		return word

	let maxIndex = nearestSqrt(word.length)
	let minIndex = 0

	let matrix = [[""]]
	for (let index = 0; index < maxIndex; index++) {
		matrix[index] = []
	}

	let forwardIndex = [0, 0]
	const forward = () => {
		forwardIndex[1]++
		if (forwardIndex[1] == maxIndex - iterationCount - 1) {
			iterationCount++
			forwardIndex[1] = iterationCount
			forwardIndex[0]++
		}
	}
	
	let downIndex = [0, maxIndex - 1]
	const down = () => {
		downIndex[0]++
		
		if (downIndex[0] == maxIndex - iterationCount) {
			downIndex[0] = iterationCount
			downIndex[1]--
		}
	}
	let backwardIndex = [maxIndex - 1, maxIndex - 1]
	const backward = () => {
		backwardIndex[1]--
		
		if (backwardIndex[1] == minIndex) {
			backwardIndex[1] = maxIndex - iterationCount - 1
			backwardIndex[0]--
		}
	}
	let upIndex = [maxIndex - 1, 0]
	const up = () => {
		upIndex[0]--
		if (upIndex[0] == minIndex) {
			minIndex++
			upIndex[0] = maxIndex - iterationCount - 1
			upIndex[1]++
		}
	}

	let iterationStep = 0
	let iterationCount = 0
	let tempArray = []

	for (let index = 0; index < maxIndex ** 2; index++) {

		switch (iterationStep) {
			case 0:
				iterationStep++
				tempArray = matrix[forwardIndex[0]]
				tempArray[forwardIndex[1]] = word[index] || " "
				matrix[forwardIndex[0]] = tempArray
				forward()
				break

			case 1:
				iterationStep++
				tempArray = matrix[downIndex[0]]
				tempArray[downIndex[1]] = word[index] || " "
				matrix[downIndex[0]] = tempArray
				down()
				break

			case 2:
				iterationStep++
				tempArray = matrix[backwardIndex[0]]
				tempArray[backwardIndex[1]] = word[index] || " "
				matrix[backwardIndex[0]] = tempArray
				backward()
				break

			case 3:
				iterationStep = 0
				tempArray = matrix[upIndex[0]]
				tempArray[upIndex[1]] = word[index] || " "
				matrix[upIndex[0]] = tempArray
				up()
				
				break

			default:
				break
		}

	}
	let returnArray = []
	for (let index = 0; index < matrix.length; index++) {
		let section = matrix[index].join("")
		returnArray.push(section)
	}

	return returnArray.join("")
}

const nearestSqrt = (n: number) => {
	let i = 1
	while (i ** 2 < n)
		i++

	return i
}